FROM python:3.6-alpine

ENV CONFIG_MODULE_FILE /coding_challenge/config.py

COPY coding_challenge_py /coding_challenge
WORKDIR /coding_challenge
RUN pip install .
EXPOSE 8080

ENTRYPOINT ["python3", "-m", "coding_challenge_py.wsgi"]
