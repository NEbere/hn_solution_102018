## Morressier Python Coding Challenge

Thank you for taking the time to do our challenge!

The goal of this challenge is for you to show us what you can do technically, and also to guide us through your thought process behind the decisions you make.

Feel free to add notes of anything you'd do differently with more time available in another .md file in this repository.

Have fun!

### Preface

- This project already contains all the python requirements you need, no need to add anything else.
- This is a Python3 project, please don't try to change that
- We prefer to use a couple of well-known Python modules like Flask, Flask-RESTful, pytest, blinker and others.
- When you're finished please push your branch to `{{initials}}_solution_{{MMYYYY}}`

### Where am I?
In this repository you will find a *REST* web application. It provides a http/json based interface to organise Authors, Organisers and Posters. 

You will also find a various amount of test cases and tools to deploy this app locally using Docker.`docker-compose up`

```
.
├── coding_challenge_py
│   ├── coding_challenge_py
│   │   ├── application.py
│   │   ├── endpoints
│   │   │   └── v1
│   │   │       ├── __init__.py
│   │   │       ├── poster_endpoint.py
│   │   │       └── users_endpoint.py
│   │   ├── __init__.py
│   │   ├── models
│   │   │   ├── __init__.py
│   │   │   ├── poster.py
│   │   │   └── user.py
│   │   ├── read_config.py
│   │   └── wsgi.py
│   ├── config.py
│   ├── dev-requirements.txt
│   ├── requirements.txt
│   ├── setup.cfg
│   ├── setup.py
│   └── tests
│       ├── conftest.py
│       ├── endpoints
│       │   └── v1
│       │       ├── test_poster_endpoint.py
│       │       └── test_user_endpoint.py
│       ├── models
│       │   └── test_signals.py
│       ├── test_application.py
│       └── test_read_config.py
├── docker-compose.yml
├── Dockerfile
└── README.md
```

### What do I need to do?
- There are a couple of bugs and unfinished work waiting for you, check out the content of the *coding_challenge_py* folder
- Our users want to be able to organise Presentationsessions, which is a Timeperiod where an arbitrary number of Authors can present their Posters to an audience.   

Please **extend** the current project and provide them with a RESTful API.
They should be able to:

- get, create and delete sessions
- add posters and speaker(author) to a sessions

### Where should I start?
We highly recommend getting familiar with the folder structure above, it will help you finding your way through this challenge.  
- Install the project locally: `pip install -e coding_challenge_py`  
- Tests are accessible using: `python coding_challenge_py/setup.py test`  
- The development server can be run using: `CONFIG_TYPE=development CONFIG_MODULE_FILE=./coding_challenge_py/config.py python coding_challenge_py/coding_challenge_py/wsgi.py`  

### What are you looking for?
- REST
- Python 3
- Tests
- Style
