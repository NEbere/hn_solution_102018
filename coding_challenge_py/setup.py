#!/usr/bin/env python

import os
import subprocess
import sys
from setuptools import setup, find_packages

from setuptools.command.test import test as TestCommand

class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]
    def initialize_options(self):
         TestCommand.initialize_options(self)
         self.pytest_args = []

    def run_tests(self):
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

setup(name='coding_challenge_py',
      version='0.1',
      description='A Morressier coding challenge for future team buddys.',
      author='The Morressier Team',
      author_email='tech@morressier.com',
      packages=find_packages(),
      install_requires=[
          'flask==0.12.2',
          'flask_restful==0.3.6',
          'mongoengine==0.13.0',
          'blinker==1.4'
      ],
      tests_require=['pytest',
                     'mongomock'],
      cmdclass={'test': PyTest}
      )
