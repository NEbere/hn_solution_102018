from mongoengine import (
    signals,
    Document,
    StringField,
    ListField,
    ReferenceField,
    DateTimeField,
    CASCADE
)

import datetime

class Session(Document):
    title = StringField(max_length=100)
    start_time = DateTimeField(default=datetime.datetime.utcnow)
    end_time = DateTimeField(default=datetime.datetime.utcnow)
    authors = ListField(ReferenceField('User', reverse_delete_rule=CASCADE), default=list)

    def to_json(self):
        return {'id': str(self.id),
                'title': self.title,
                'start_time': str(self.start_time),
                'end_time': str(self.end_time),
                'authors': [str(author.id) for author in self.authors]}
