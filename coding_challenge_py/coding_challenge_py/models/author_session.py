from mongoengine import (
    signals,
    Document,
    ReferenceField,
    CASCADE,
)

class AuthorSession(Document):
    author = ReferenceField('User',reverse_delete_rule=CASCADE)
    session = ReferenceField('Session',reverse_delete_rule=CASCADE)

    def to_json(self):
        return {'id': str(self.id),
                'author': str(self.author.id),
                'session': str(self.session.id)
                }

    @classmethod
    def add_updates_to_author_and_session(cls, sender, document, created, **kwargs):
        document.author.update(add_to_set__sessions=document.session)
        document.session.update(add_to_set__authors=document.author)

    @classmethod
    def remove_author_and_session(cls, sender, document, **kwargs):
        document.author.update(pull__sessions=document.session)
        document.session.update(pull__authors=document.author)

signals.post_save.connect(AuthorSession.add_updates_to_author_and_session, sender=AuthorSession)
signals.pre_delete.connect(AuthorSession.remove_author_and_session, sender=AuthorSession)
