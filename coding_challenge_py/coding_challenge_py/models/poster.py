from mongoengine import (
    signals,
    Document,
    StringField,
    ReferenceField,
    CASCADE,
)


class Poster(Document):
    title = StringField(max_length=100)
    author = ReferenceField('User',
                            reverse_delete_rule=CASCADE)

    def to_json(self):
        return {'id': str(self.id),
                'title': self.title,
                'author': str(self.author.id)}

    @classmethod
    def add_poster_to_author(cls, sender, document, created, **kwargs):
        document.author.update(add_to_set__posters=document)

    @classmethod
    def remove_poster_from_author(cls, sender, document, **kwargs):
        document.author.update(pull__posters=document)


signals.post_save.connect(Poster.add_poster_to_author, sender=Poster)
signals.pre_delete.connect(Poster.remove_poster_from_author, sender=Poster)
