from mongoengine import (
    Document,
    EmailField,
    StringField,
    ListField,
    ReferenceField,
)

TITLES = ('Mr.', 'Ms.', 'Dr.', 'Prof.')
ROLES = ('author', 'organiser')


class User(Document):
    email = EmailField(required=True, unique=True)
    title = StringField(max_length=5, choices=TITLES)
    name = StringField(max_length=56)
    roles = ListField(StringField(choices=ROLES), default=['author'])
    posters = ListField(ReferenceField('Poster'), default=list)
    sessions = ListField(ReferenceField('Session'), default=list)

    def to_json(self):
        return {'id': str(self.id),
                'email': self.email,
                'title': self.title,
                'name': self.name,
                'roles': self.roles,
                'posters': [str(poster.id) for poster in self.posters],
                'sessions': [str(session.id) for session in self.sessions]}
