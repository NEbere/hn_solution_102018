from flask import Blueprint
from flask_restful import Api

from coding_challenge_py.endpoints.v1.users_endpoint import UsersEndpoint
from coding_challenge_py.endpoints.v1.poster_endpoint import PosterEndpoint

from coding_challenge_py.endpoints.v1.session_endpoint import SessionEndpoint

from coding_challenge_py.endpoints.v1.author_session_endpoint import AuthorSessionEndpoint

api_bp = Blueprint('api_bp', __name__)
api = Api(api_bp, catch_all_404s=True)

api.add_resource(UsersEndpoint,
                 '/users',
                 '/users/<string:id>',
                 endpoint='users')

api.add_resource(PosterEndpoint,
                 '/posters',
                 '/posters/<string:id>',
                 endpoint='posters')

api.add_resource(SessionEndpoint,
                 '/sessions',
                 '/sessions/<string:id>',
                 endpoint='sessions')

api.add_resource(AuthorSessionEndpoint,
                 '/authors/sessions',
                 '/authors/sessions/<string:id>',
                 endpoint='authors_session')
