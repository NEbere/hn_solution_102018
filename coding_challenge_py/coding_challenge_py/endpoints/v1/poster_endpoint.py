from bson import json_util, ObjectId
from flask_restful import Resource, reqparse
from flask import abort
from mongoengine.errors import ValidationError

from coding_challenge_py.models import Poster, User


class PosterEndpoint(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title',
                                   location='json')
        self.reqparse.add_argument('author',
                                   location='json',
                                   type=ObjectId)

    def get(self, id=None):
        if not id:
            return [poster.to_json() for poster in Poster.objects()]

        id_length = len(id)
        if id_length != 24:
            abort(400, 'Poster id, it must be a 12-byte input or a 24-character hex string')
        
        try:
            poster = Poster.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid PosterId')
        if not poster:
            abort(404, 'No Poster Found')
        return poster.to_json()

    def post(self, id=None):
        if id:
            abort(400, 'POST with ID is not allowed')
        args = self.reqparse.parse_args(strict=True)

        poster = Poster(title=args.get('title'),
                        author=args.get('author')).save()

        return poster.to_json()

    def delete(self, id):
        id_length = len(id)
        if id_length != 24:
            abort(400, 'Poster id, it must be a 12-byte input or a 24-character hex string')
        
        poster = Poster.objects(id=id).first()
        if not poster:
            abort(404, 'No Poster Found')
        poster.delete()
        return 200
