from flask import abort, Response
from mongoengine.errors import ValidationError, NotUniqueError
from flask_restful import Resource, reqparse

from coding_challenge_py.models import User
from coding_challenge_py.models.user import TITLES, ROLES


def check_for_title(title):
    if title not in TITLES:
        raise TypeError(f'{title} needs to be one of {TITLES}')
    return title


def check_for_roles(role):
    if role not in ROLES:
        raise TypeError(f'Got role {role} but needs to be one of: {ROLES})')
    return role


class UsersEndpoint(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email',
                                   required=True,
                                   location='json')
        self.reqparse.add_argument('title',
                                   location='json',
                                   type=check_for_title)
        self.reqparse.add_argument('roles',
                                   location='json',
                                   action='append',
                                   type=check_for_roles)
        self.reqparse.add_argument('name',
                                   location='json')

    def get(self, id=None):
        if not id:
            return [user.to_json() for user in User.objects()]

        try:
            user = User.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid UserId')
        if not user:
            abort(404)

        return user.to_json()

    def post(self, id=None):
        if id:
            abort(400, 'POST with <id> is not allowed')

        args = self.reqparse.parse_args(strict=True)

        try:
            user = User(email=args.get('email'),
                        title=args.get('title'),
                        name=args.get('name'),
                        roles=args.get('roles'),
                        ).save()
            return user.to_json()
        except NotUniqueError as e:
            abort(400, 'User already existing.')
        return user.to_json()

    def delete(self, id):
        try:
            user = User.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid UserId')
        if not user:
            abort(404)
        user.delete()
