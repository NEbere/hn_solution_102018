from flask import abort, Response
from mongoengine.errors import ValidationError, NotUniqueError
from flask_restful import Resource, reqparse

from coding_challenge_py.models import Session


class SessionEndpoint(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title',
                                   location='json',
                                   required=True)
        self.reqparse.add_argument('start_time',
                                   location='json',
                                   required=True)
        self.reqparse.add_argument('end_time',
                                   location='json',
                                   required=True)

    def get(self, id=None):
        if not id:
            return [session.to_json() for session in Session.objects()]

        try:
            session = Session.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid sessionId')
        if not session:
            abort(404)

        return session.to_json()

    def post(self, id=None):
        if id:
            abort(400, 'POST with <id> is not allowed')

        args = self.reqparse.parse_args(strict=True)

        try:
            session = Session(title=args.get('title'),
                        start_time=args.get('start_time'),
                        end_time=args.get('end_time')
                        ).save()
            return session.to_json()
        except NotUniqueError as e:
            abort(400, 'session already existing.')
        return session.to_json()

    def delete(self, id):
        try:
            session = Session.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid sessionId')
        if not session:
            abort(404, 'Session not found')
        session.delete()
