from bson import json_util, ObjectId
from flask_restful import Resource, reqparse
from flask import abort
from mongoengine.errors import ValidationError

from coding_challenge_py.models import AuthorSession, Session


class AuthorSessionEndpoint(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('author',
                                   location='json',
                                   type=ObjectId)

        self.reqparse.add_argument('session',
                                   location='json',
                                   type=ObjectId)

    def get(self, id=None):
        if not id:
            return [author_session.to_json() for author_session in AuthorSession.objects()]

        try:
            author_session = AuthorSession.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid author_session_id')
        if not author_session:
            abort(404, 'No Author session Found')
        return author_session.to_json()

    def post(self, id=None):
        if id:
            abort(400, 'POST with ID is not allowed')
        args = self.reqparse.parse_args(strict=True)

        author_session = AuthorSession(author=args.get('author'),
                        session=args.get('session')).save()

        return author_session.to_json()

    def delete(self, id):
        try:
            author_session = AuthorSession.objects(id=id).first()
        except ValidationError:
            abort(400, 'Not a valid author_session_id')
        if not author_session:
            abort(404, 'Not found')
        author_session.delete()
        return 200
