import os
import sys
import re
import configparser
import types


def read_config_py(filename):
    if not os.path.exists(filename):
        raise FileNotFoundError('Could not find file under given path %s',
                                filename)

    d = types.ModuleType('config')
    d.__file__ = filename

    try:
        with open(filename) as config_file:
            exec(compile(config_file.read(), filename, 'exec'), d.__dict__)
    except IOError as e:
        e.strerror = 'Unable to load configuration file (%s)' % e.strerror
        raise

    if 'config' not in d.__dict__:
        raise ValueError('config dict not found in config module')

    if not isinstance(d.config, dict):
        raise TypeError('config needs to be of type dictionary')

    return d
