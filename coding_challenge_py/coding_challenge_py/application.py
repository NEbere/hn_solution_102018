import os

from flask import Flask
import mongoengine

from coding_challenge_py.read_config import read_config_py
from coding_challenge_py.endpoints import v1


def create_app(config_module_file=os.getenv('CONFIG_MODULE_FILE'),
               config_type=os.getenv('CONFIG_TYPE', 'production')):
    app = Flask(__name__)

    if not config_module_file:
        raise ValueError('You need to specify a config module')

    config_module = read_config_py(config_module_file)
    config = config_module.config[config_type]

    for key in config.flask:
        app.config[key] = config.flask[key]

    mongoengine.connect(host=config.mongodb['host'])

    app.register_blueprint(v1.api_bp)
    return app
