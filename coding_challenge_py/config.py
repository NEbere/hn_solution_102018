import os


class DevelopmentConfig():
    mongodb = {'host': 'mongodb://localhost:27017'}
    flask = {'DEBUG': True}


class TestingConfig():
    mongodb = {'host': 'mongomock://localhost:27017'}
    flask = {'DEBUG': False}


class ProductionConfig():
    mongodb = {'host': 'mongodb://'}
    flask = {'DEBUG': False}


class ComposeConfig():
    mongodb = {'host': 'mongodb://mongodb:27017'}
    flask = {'DEBUG': True}


config = {
    'testing': TestingConfig(),
    'development': DevelopmentConfig(),
    'production': ProductionConfig(),
    'docker_compose': ComposeConfig(),
}
