import json
import pytest

from coding_challenge_py.models import User, Poster

from tests.client import client

POSTER_ENDPOINT = '/posters'

testdata_delete = [
    ('foo', 400),
    ('507f1f77bcf86cd799439011', 404)
]


def setup_module():
    user = User('foo@bar.com').save()
    poster = Poster('awesome poster', user).save()


def teardown_module():
    User.objects().delete()
    Poster.objects().delete()


def test_methods(client):
    ret = client.options(POSTER_ENDPOINT)
    allowed = ret.headers['Allow'].split(', ')
    assert 'HEAD' in allowed
    assert 'GET' in allowed
    assert 'POST' in allowed
    assert 'OPTIONS' in allowed
    assert 'DELETE' in allowed
    assert len(allowed) == 5


def test_invalid_id(client):
    ret = client.get(POSTER_ENDPOINT + '/foo')
    assert ret.status_code == 400


def test_empty_return(client):
    ret = client.get(POSTER_ENDPOINT + '/' + '596892ead462b826deadbeef')
    assert ret.status_code == 404


def test_get_single_poster(client):
    poster_db = Poster.objects().first()
    ret = client.get(POSTER_ENDPOINT + '/' + str(poster_db.id))

    assert ret.status_code == 200
    poster = json.loads(ret.get_data())
    assert poster['id'] == str(poster_db.id)
    assert poster['title'] == poster_db.title
    assert poster['author'] == str(poster_db.author.id)


def test_get_single_poster_from_list(client):
    poster_db = Poster.objects().first()
    ret = client.get(POSTER_ENDPOINT)
    posters = json.loads(ret.get_data())

    assert ret.status_code == 200
    assert len(posters) == 1
    poster = posters[0]
    assert poster['id'] == str(poster_db.id)
    assert poster['title'] == poster_db.title
    assert poster['author'] == str(poster_db.author.id)


def test_get_multiple_posters(client):
    user_db = User.objects().first()
    poster_db_1 = Poster.objects().first()
    poster_db_2 = Poster('owefull poster', user_db).save()

    ret = client.get(POSTER_ENDPOINT)

    assert ret.status_code == 200
    posters = json.loads(ret.get_data())
    assert len(posters) == 2


def test_not_a_user(client):
    payload = json.dumps({'title': 'HOTHOTHOT', 'author': 'NotAUser'})
    ret = client.post(POSTER_ENDPOINT,
                      data=payload,
                      content_type='application/json')
    assert ret.status_code == 400


def test_post_simple(client):
    user_db = User.objects().first()
    payload = json.dumps({'title': 'HOTHOTHOT', 'author': str(user_db.id)})

    ret = client.post(POSTER_ENDPOINT,
                      data=payload,
                      content_type='application/json')
    assert ret.status_code == 200

    poster = Poster.objects(title='HOTHOTHOT').first()
    assert poster
    assert poster.title == 'HOTHOTHOT'
    assert poster.author == user_db
    user_db.reload()
    assert len(user_db.posters) == 3


def test_delete(client):
    user = User.objects().first()
    poster = Poster('awesome poster', user).save()
    user.reload()

    assert poster in user.posters
    ret = client.delete(POSTER_ENDPOINT + '/' + str(poster.id))
    assert ret.status_code == 200

    assert Poster.objects(id=str(poster.id)).first() is None


@pytest.mark.parametrize('id, expected', testdata_delete)
def test_delete_id_not_found(client, id, expected):
    ret = client.delete(POSTER_ENDPOINT + '/' + id)
    assert ret.status_code == expected
