import json
import datetime
import pytest

from coding_challenge_py.models import Session
from tests.client import client


SESSION_ENDPOINT = '/sessions'

def setup_module():
    start_time = datetime.datetime.strptime('2018-10-29 05:30:11.813557', '%Y-%m-%d %H:%M:%S.%f')
    end_time = datetime.datetime.strptime('2018-10-29 09:30:11.813557', '%Y-%m-%d %H:%M:%S.%f')
    Session.objects().delete()
    session = Session('session title', start_time, end_time).save()


def teardown_module():
    Session.objects().delete()


def test_methods(client):
    ret = client.options(SESSION_ENDPOINT)
    allowed = ret.headers['Allow'].split(', ')
    assert 'HEAD' in allowed
    assert 'GET' in allowed
    assert 'OPTIONS' in allowed
    assert 'POST' in allowed
    assert 'DELETE' in allowed
    assert len(allowed) == 5


def test_invalid_id(client):
    ret = client.get(SESSION_ENDPOINT + '/foo')
    assert ret.status_code == 400


def test_empty_return(client):
    ret = client.get(SESSION_ENDPOINT + '/' + '596892ead462b826deadbeef')
    assert ret.status_code == 404


def test_get_single_session(client):
    session_db = Session.objects().first()
    ret = client.get(SESSION_ENDPOINT + '/' + str(session_db.id))

    assert ret.status_code == 200
    session = json.loads(ret.get_data())
    assert session['id'] == str(session_db.id)
    assert session['title'] == session_db.title
    assert datetime.datetime.strptime(session['start_time'], '%Y-%m-%d %H:%M:%S.%f') == session_db.start_time
    assert datetime.datetime.strptime(session['end_time'], '%Y-%m-%d %H:%M:%S.%f') == session_db.end_time


def test_get_single_session_from_list(client):
    session_db = Session.objects().first()
    ret = client.get(SESSION_ENDPOINT + '/' + str(session_db.id))

    assert ret.status_code == 200
    session = json.loads(ret.get_data())
    assert session['id'] == str(session_db.id)
    assert session['title'] == session_db.title


def test_get_multiple_sessions(client):
    session_db = Session.objects().first()
    session_db1 = Session('session 1').save()
    session_db3 = Session('session 2').save()

    ret = client.get(SESSION_ENDPOINT)

    assert ret.status_code == 200
    sessions = json.loads(ret.get_data())
    assert len(sessions) == 3

def test_post_simple(client):
    payload = json.dumps({'title': 'Test session',
                        'start_time': '2018-10-29 05:30:11.813557',
                        'end_time': '2018-10-29 09:30:11.813557'})

    ret = client.post(SESSION_ENDPOINT, data=payload,
                      content_type='application/json')
    session = json.loads(ret.get_data())
    assert ret.status_code == 200
    assert session['title'] == 'Test session'
    Session.objects(id=session['id']).delete()


def test_delete(client):
    session = Session.objects().first()
    ret = client.delete(SESSION_ENDPOINT + '/' + str(session.id))
    assert ret.status_code == 200
    assert Session.objects(id=str(session.id)).first() is None

