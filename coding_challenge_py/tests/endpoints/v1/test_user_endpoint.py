import json

import pytest

from coding_challenge_py.models import User, Poster
from tests.client import client


USER_ENDPOINT = '/users'

testdata_roles = [
    (['author', 'organiser'], 200),
    (['author'], 200),
    ('author', 200),
    ('foo', 400),
    (['foo'], 400),
    (['author', 'foo'], 400),
    (['author', 12], 400)
]

testdata_titles = [
    ('Dr.', 200),
    ('Prof.', 200),
    ('Mr.', 200),
    ('Mr', 400),
    ('foo.', 400),
    (['Mr.', 'foo'], 400),
]


testdata_delete = [
    ('foo', 400),
    ('507f1f77bcf86cd799439011', 404)
]


def setup_module():
    User.objects().delete()
    user = User('foo@bar.com').save()
    poster = Poster('awesome poster', user).save()


def teardown_module():
    User.objects().delete()
    Poster.objects().delete()


def test_methods(client):
    ret = client.options(USER_ENDPOINT)
    allowed = ret.headers['Allow'].split(', ')
    assert 'HEAD' in allowed
    assert 'GET' in allowed
    assert 'OPTIONS' in allowed
    assert 'POST' in allowed
    assert 'DELETE' in allowed
    assert len(allowed) == 5


def test_invalid_id(client):
    ret = client.get(USER_ENDPOINT + '/foo')
    assert ret.status_code == 400


def test_empty_return(client):
    ret = client.get(USER_ENDPOINT + '/' + '596892ead462b826deadbeef')
    assert ret.status_code == 404


def test_get_single_user(client):
    user_db = User.objects().first()
    ret = client.get(USER_ENDPOINT + '/' + str(user_db.id))

    assert ret.status_code == 200
    user = json.loads(ret.get_data())
    assert user['id'] == str(user_db.id)
    assert user['email'] == user_db.email
    assert user['title'] == user_db.title
    assert user['name'] == user_db.name
    assert user['roles'] == user_db.roles
    assert user['posters'] == [str(poster.id) for poster in user_db.posters]


def test_get_single_user_from_list(client):
    user_db = User.objects().first()
    ret = client.get(USER_ENDPOINT + '/' + str(user_db.id))

    assert ret.status_code == 200
    user = json.loads(ret.get_data())
    assert user['id'] == str(user_db.id)
    assert user['email'] == user_db.email


def test_get_multiple_users(client):
    user_db = User.objects().first()
    user_db1 = User('foo2@bar.com').save()
    user_db3 = User('foo3@bar.com').save()

    ret = client.get(USER_ENDPOINT)

    assert ret.status_code == 200
    users = json.loads(ret.get_data())
    assert len(users) == 3


@pytest.mark.parametrize('title, expected', testdata_titles)
def test_title_validation(client, title, expected):
    payload = {'email': 'someone@bar.com', 'title': title}
    ret = client.post(USER_ENDPOINT,
                      data=json.dumps(payload),
                      content_type='application/json')
    assert ret.status_code == expected
    if expected == 200:
        user = json.loads(ret.get_data())
        user['title'] == title
    User.objects(email='someone@bar.com').delete()


@pytest.mark.parametrize('role, expected', testdata_roles)
def test_roles_validation(client, role, expected):
    payload = {'email': 'someone@bar.com', 'roles': role}
    ret = client.post(USER_ENDPOINT,
                      data=json.dumps(payload),
                      content_type='application/json')

    assert ret.status_code == expected
    if expected == 200:
        user = json.loads(ret.get_data())
        user['roles'] == role
    User.objects(email='someone@bar.com').delete()


def test_post_simple(client):
    payload = json.dumps({'email': 'foodieBarson@paris.com',
                          'title': 'Mr.',
                          'name': 'Foodie Barson',
                          'roles': ['author', 'organiser']})

    ret = client.post(USER_ENDPOINT,
                      data=payload,
                      content_type='application/json')
    user = json.loads(ret.get_data())
    assert ret.status_code == 200
    assert user['email'] == 'foodieBarson@paris.com'
    assert user['title'] == 'Mr.'
    assert user['name'] == 'Foodie Barson'
    assert user['roles'] == ['author', 'organiser']
    User.objects(email='foodieBarson@paris.com').delete()


def test_delete(client):
    user = User.objects().first()
    ret = client.delete(USER_ENDPOINT + '/' + str(user.id))
    assert ret.status_code == 200
    assert User.objects(id=str(user.id)).first() is None


@pytest.mark.parametrize('id, expected', testdata_delete)
def test_delete_id_not_found(client, id, expected):
    ret = client.delete(USER_ENDPOINT + '/' + id)
    assert ret.status_code == expected


def test_delete_cascading(client):
    user = User('someother@bar.com').save()
    poster = Poster('awesome poster', user).save()
    assert len(User.objects(id=str(user.id))) == 1
    assert len(Poster.objects(author=str(user.id))) == 1

    ret = client.delete(USER_ENDPOINT + '/' + str(user.id))
    assert ret.status_code == 200
    assert User.objects(id=str(user.id)).first() is None
    assert Poster.objects(author=str(user.id)).first() is None
