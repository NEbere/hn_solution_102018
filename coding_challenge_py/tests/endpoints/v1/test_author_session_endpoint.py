import json
import pytest
import datetime

from coding_challenge_py.models import User, Session, AuthorSession
from tests.client import client

AUTHOR_SESSION_ENDPOINT = '/authors/sessions'
SESSION_ENDPOINT = '/sessions'

def setup_module():
    start_time = datetime.datetime.strptime('2018-10-29 05:30:11.813557', '%Y-%m-%d %H:%M:%S.%f')
    end_time = datetime.datetime.strptime('2018-10-29 09:30:11.813557', '%Y-%m-%d %H:%M:%S.%f')
    User.objects().delete()
    Session.objects().delete()
    AuthorSession.objects().delete()

    session = Session('session title', start_time, end_time).save()
    user = User('foo@bar.com').save()
    author_session = AuthorSession(user.id, session.id).save()

def teardown_module():
    User.objects().delete()
    Session.objects().delete()
    AuthorSession.objects().delete()


def test_methods(client):
    ret = client.options(AUTHOR_SESSION_ENDPOINT)
    allowed = ret.headers['Allow'].split(', ')
    assert 'HEAD' in allowed
    assert 'GET' in allowed
    assert 'OPTIONS' in allowed
    assert 'POST' in allowed
    assert 'DELETE' in allowed
    assert len(allowed) == 5


def test_invalid_id(client):
    ret = client.get(AUTHOR_SESSION_ENDPOINT + '/foo')
    assert ret.status_code == 400


def test_empty_return(client):
    ret = client.get(AUTHOR_SESSION_ENDPOINT + '/' + '596892ead462b826deadbeef')
    assert ret.status_code == 404


def test_get_single_author_session(client):
    db_data = AuthorSession.objects().first()
    ret = client.get(AUTHOR_SESSION_ENDPOINT + '/' + str(db_data.id))

    assert ret.status_code == 200
    session = json.loads(ret.get_data())
    assert session['id'] == str(db_data.id)

    user_data = db_data.author.to_json()
    session_data = db_data.session.to_json()
    assert session['id'] == str(db_data.id)
    assert session['author'] == user_data['id']
    assert session['session'] == session_data['id']


def test_get_single_author_session_from_list(client):
    db_data = AuthorSession.objects().first()
    ret = client.get(AUTHOR_SESSION_ENDPOINT + '/' + str(db_data.id))

    assert ret.status_code == 200
    session = json.loads(ret.get_data())
    user_data = db_data.author.to_json()
    session_data = db_data.session.to_json()

    assert session['id'] == str(db_data.id)
    assert session['author'] == user_data['id']
    assert session['session'] == session_data['id']


def test_get_multiple_users(client):
    start_time = datetime.datetime.strptime('2018-10-29 05:30:11.813557', '%Y-%m-%d %H:%M:%S.%f')
    end_time = datetime.datetime.strptime('2018-10-29 09:30:11.813557', '%Y-%m-%d %H:%M:%S.%f')
    
    db_data = AuthorSession.objects().first()
    user_db2 = User('foo2@bar.com').save()
    session2 = Session('session title', start_time, end_time).save()
    author_session = AuthorSession(user_db2.id, session2.id).save()

    ret = client.get(AUTHOR_SESSION_ENDPOINT)
    assert ret.status_code == 200
    sessions = json.loads(ret.get_data())
    assert len(sessions) == 2


def test_post(client):
    user = User('foodieBarson@paris.com').save()
    session = Session('session title', datetime.datetime.now(), datetime.datetime.now()).save()
    payload = json.dumps({'author': str(user.id), 'session': str(session.id)})

    ret = client.post(AUTHOR_SESSION_ENDPOINT, data=payload,
                      content_type='application/json')

    author_session = json.loads(ret.get_data())
    assert ret.status_code == 200
    assert author_session['author'] == str(user.id)
    assert author_session['session'] == str(session.id)

    AuthorSession.objects(id=author_session['id']).delete()


def test_delete(client):
    author_session = AuthorSession.objects().first()
    ret = client.delete(AUTHOR_SESSION_ENDPOINT + '/' + str(author_session.id))
    assert ret.status_code == 200
    assert AuthorSession.objects(id=str(author_session.id)).first() is None

def test_create_cascading(client):
    user = User('someother@paris.com').save()
    session = Session('session title - test', datetime.datetime.now(), datetime.datetime.now()).save()
    author_session = AuthorSession(user.id, session.id).save()

    assert len(User.objects(id=str(user.id))) == 1
    assert len(Session.objects(id=str(session.id))) == 1

    author_session_json = author_session.to_json()
    author = User.objects(id=author_session_json['author']).first().to_json()
    session = Session.objects(id=author_session_json['session']).first().to_json()

    assert author['sessions'][0] == author_session_json['session']
    assert session['authors'][0] == author_session_json['author']

def test_delete_session_cascading(client):
    user = User('someother1@paris.com').save()
    session = Session('session title - test', datetime.datetime.now(), datetime.datetime.now()).save()
    author_session = AuthorSession(user.id, session.id).save()

    ret = client.delete(SESSION_ENDPOINT + '/' + str(session.id))
    assert ret.status_code == 200
    assert Session.objects(id=str(session.id)).first() is None
    assert AuthorSession.objects(id=str(author_session.id)).first() is None

    user_object = User.objects(email='someother1@paris.com').first().to_json()
    assert not(user_object['sessions'])



def test_delete_cascading(client):
    user = User('someother2@paris.com').save()
    session = Session('session title - test', datetime.datetime.now(), datetime.datetime.now()).save()
    author_session = AuthorSession(user.id, session.id).save()

    ret = client.delete(AUTHOR_SESSION_ENDPOINT + '/' + str(author_session.id))
    assert ret.status_code == 200
    assert AuthorSession.objects(id=str(author_session.id)).first() is None

    user_object = User.objects(id=str(user.id)).first().to_json()
    session_object = Session.objects(id=str(session.id)).first().to_json()

    assert not(user_object['sessions'])
    assert session_object is not None
    assert str(user_object['id']) not in session_object['authors']

