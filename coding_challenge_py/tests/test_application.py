from coding_challenge_py.application import create_app

from pytest import raises


def test_basic_app_creation():
    create_app()


def test_db_connection():
    app = create_app()
