import os

from pytest import raises
from coding_challenge_py.read_config import read_config_py


BROKEN_CONFIG = 'broken_config'
WORKING_CONFIG = 'working_config'


def setup_module(module):
    pass


def teardown_module(module):
    os.remove(BROKEN_CONFIG)
    os.remove(WORKING_CONFIG)


def test_basic():
    with raises(FileNotFoundError):
        read_config_py('NoFileHere')


def test_broken_config():
    with open(BROKEN_CONFIG, 'w') as file:
        file.write('thisIsNotAPythonCommand')

    with raises(NameError):
        read_config_py(BROKEN_CONFIG)


def test_working_python():
    with open(WORKING_CONFIG, 'w') as file:
        file.write('')

    with raises(ValueError):
        read_config_py(WORKING_CONFIG)


def test_config_type():
    with open(WORKING_CONFIG, 'w') as file:
        file.write("config = 'foo'")

    with raises(TypeError):
        read_config_py(WORKING_CONFIG)

    with open(WORKING_CONFIG, 'w') as file:
        file.write("config = {}")

    read_config_py(WORKING_CONFIG)


def test_config_not_a_object():
    with open(WORKING_CONFIG, 'w') as file:
        file.write('''
config = {'test': 'foo'}
                   ''')

    read_config_py(WORKING_CONFIG)


def test_config_classes():
    with open(WORKING_CONFIG, 'w') as file:
        file.write('''
class DevelopmentConfig():
    mongodb = {
        '':  '',
    }

    flask = {
        'URL_PREFIX': '/',
    }


config = {'test': DevelopmentConfig()}
                   ''')
    d = read_config_py(WORKING_CONFIG)
