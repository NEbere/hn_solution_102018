from coding_challenge_py.models import Poster, User


def setup_module():
    user = User('foo@bar.com').save()
    poster = Poster('awesome poster', user).save()


def teardown_module():
    User.objects().delete()
    Poster.objects().delete()


def test_poster_creation():
    """
    Tests that poster creation adds poster to author
    """
    user = User.objects().first()
    poster = Poster.objects().first()

    assert poster in user.posters


def test_poster_deletion():
    """
    Tests that poster reference gets removed when poster is deleted
    """
    user = User.objects().first()
    poster = Poster.objects().first()

    poster.delete()

    assert len(Poster.objects(id=poster.id)) == 0, 'Poster not deleted'
    user.reload()
    assert poster not in user.posters, 'Poster not removed from user'
    assert len(user.posters) == 0, 'Users poster reference should be empty'
