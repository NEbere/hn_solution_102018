import os

import pytest

from coding_challenge_py import create_app
from coding_challenge_py.models import User


CWD = os.path.dirname(os.path.realpath(__file__))
USER_ID = "566821242313886deea14284"


@pytest.fixture(scope='session')
def app(request):
    return create_app(CWD + '/../config.py', 'testing')


@pytest.fixture()
def author():
    return User(id=ObjectId(USER_ID),
                email='foo@bar.com',
                title='Mr.',
                firstname='foo',
                lastname='bar',
                roles=['author']).save()


@pytest.fixture()
def posters():
    return Poster('A awesome Poster',
                  author).save()
