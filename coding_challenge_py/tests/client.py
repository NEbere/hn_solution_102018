import os
import pytest

from coding_challenge_py import create_app
CWD = os.path.dirname(os.path.realpath(__file__))

@pytest.fixture()
def client():
    app = create_app(CWD + '/../config.py', 'testing')
    client = app.test_client()
    return client