# Note

## Implemented

- Implemented atomic updates on Poster
- Fixed bugs and failing tests and TODO in code
- Created endpoints [POST, GET, DELETE] for session
- Created endpoints [POST, GET, DELETE] for author_session (Linking document for authors and sessions)

## Model

Created a linking model `AuthorSession` to handle the many-to-many relationship
betweeen User and Session. A User can belong to multiple sessions and Session can be attributed to multiple users.

Poster is added to User then the user can be added to one or more sessions using the linking document that can be accessed via the endpoint `/authors/sessions`

When a linking document is deleted, the session is removed from the users `sessions` array and the author is also removed from the sessions `authors` array.

Also, when a session is deleted, all AuthorSession documents attributed to that session is deleted and the session is removed from the users `sessions`.

## TODO

- add more test to Session and Author Session
- Fix mongoengine warnings while running test
- implement validation for query strings and improve error handling for different scenarios

## Run and Test

- to start server: start mongoBD server

run  `pip install -e coding_challenge_py`, when it completes,
run `ONFIG_TYPE=development CONFIG_MODULE_FILE=./coding_challenge_py/config.py python coding_challenge_py/coding_challenge_py/wsgi.py`

- to test, run `CONFIG_TYPE=testing CONFIG_MODULE_FILE=./coding_challenge_py/config.py python coding_challenge_py/setup.py test`

## Challenges

- Due to the structure of the Session model and AuthorSession models

The client might need to make multiple calls to get different authors for a session or sessions for an author, and even posters for an authoir.

This is because the relationships can't be expressed as embedded documents as they need to be accessible outside of these relationships.

Also, then this creates the needs to look into creating databse queries with JOINS with Mongoengine if this is to be tackled from the backend

## Tools

- Python 3.6.5
- MongoDB shell version v4.0.0